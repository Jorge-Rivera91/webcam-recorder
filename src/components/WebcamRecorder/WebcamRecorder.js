
import { DownloadOutlined } from '@ant-design/icons';
import { Button } from 'antd';
import Title from 'antd/lib/skeleton/Title';
import { useEffect, useRef, useState } from 'react';
import './style.css';

const WebcamRecorder = () => {

  const videoRef = useRef(null);
  const streamRef = useRef(null); // Ya es tipo MediaStream
  const streamRecoderRef = useRef(null);

  const [error, setError] = useState('');
  const [isRecording, setIsRecording] = useState(false);
  const [audioSource, setAudioSource] = useState('');
  const [videoSource, setVideoSource] = useState('');
  const [audioSourceOptions, setAudioSourceOptions] = useState([]);
  const [videoSourceOptions, setVideoSourceOptions] = useState([]);
  const chunks = useRef([]);
  const [downloadLink, setDownloadLink] = useState('');

  const handleAudioChange = (event) => {
    setAudioSource(event.target.value)
  }

  const handleVideoChange = (event) => {
    setVideoSource(event.target.value);
  }

  const startRecording = () => {
    if (isRecording) {
      return;
    }
    
    if (!streamRef.current) {
      return;
    }
    streamRecoderRef.current = new MediaRecorder(streamRef.current);
    streamRecoderRef.current.start();
    streamRecoderRef.current.ondataavailable = function (event) {
      if (chunks.current) {
        chunks.current.push(event.data);
      }
    }
    setIsRecording(true);
  }

  const stopRecording = () => {
    if (!streamRef.current) {
      return;
    }
    streamRecoderRef.current.stop();
    setIsRecording(false);
  }

  const getDevices = async () => {
    return navigator.mediaDevices.enumerateDevices();
  }

  const gotDevices = (mediaDevices) => {
    const audioSourceOptions = [];
    const videoSourceOptions = [];
    for (const mediaDevice of mediaDevices) {
      if (mediaDevice.kind === 'audioinput') {
        audioSourceOptions.push({
          value: mediaDevice.deviceId,
          label: mediaDevice.label || `Microphone ${mediaDevice.deviceId}`
        })
      } else if (mediaDevice.kind === 'videoinput') {
        videoSourceOptions.push({
          value: mediaDevice.deviceId,
          label: mediaDevice.label || `Camera ${mediaDevice.deviceId}`
        })
      }
    }
    setAudioSourceOptions(audioSourceOptions);
    setVideoSourceOptions(videoSourceOptions);
  }

  const goStream = (stream) => {
    streamRef.current = stream;
    if (videoRef.current) {
      videoRef.current.srcObject = stream;
    }
  }

  const getStream = async () => {
    if (streamRef.current) {
      // Asegurarnos que los canales de audio y video esten disponibles
      streamRef.current.getTracks().forEach((track) => {
        track.stop();
      });
    }

    // Leer todos los mediaDevices
    const constraints = {
      audio: { deviceId: audioSource !== '' ? {exact: audioSource} : undefined },
      video: { deviceId: videoSource !== '' ? {exact: videoSource} : undefined },
    }

    try {
      const stream = await navigator.mediaDevices.getUserMedia(constraints);
      goStream(stream);
    } catch (error) {
      setError(error);
    }
  }

  const prepareStream = async () => {
    await getStream();
    const mediaDevices = await getDevices();
    gotDevices(mediaDevices);
    
  }

  useEffect(() => {
    prepareStream();
  }, [audioSource, videoSource]);

  useEffect(() => {
    if (isRecording) {
      return;
    }
    if (chunks.current.length === 0) {
      return;
    }
    
    const blob = new Blob(chunks.current, {
      type: "video/x-matroska;codecs=avc1,opus",
    });
    setDownloadLink(URL.createObjectURL(blob));
    chunks.current = [];

  }, [isRecording]);
  return (
    <div>
      <div className='video-container'>
        <div style={{
          display: 'flex',
          justifyContent:'center',
          marginBottom: '15px',
        }}>
          <select onChange={handleVideoChange} name='videoSource' value={videoSource}>
            {videoSourceOptions.map((videoOption) => (
              <option key={videoOption.value} value={videoOption.value} >{videoOption.label}</option>
            ))}
          </select>
          <select onChange={handleAudioChange} name='audioSource' value={audioSource}>
            {audioSourceOptions.map((audioOption) => (
              <option key={audioOption.value} value={audioOption.value} >{audioOption.label}</option>
            ))}
          </select>
        </div>
        <div className='video-container__video'>
          <video ref={videoRef} autoPlay muted playsInline></video>
          {error && <span>{error.message}</span> }
        </div>
        <div style={{
          display: 'flex',
          justifyContent:'center',
          marginTop:'10px',
        }}>
          <Button type="primary" loading={isRecording} onClick={startRecording}>
              Record
          </Button>
          <Button type="primary" danger disabled={!isRecording} onClick={stopRecording}>
            Stop Recording
          </Button>
        </div>
      </div>

      <Title level={2}>Preview</Title>

      <div className='video-container'>
        {downloadLink && (
          <div className='video-container__video'>
            <video src={downloadLink} controls ></video>
          </div>
        )}
        {downloadLink && (
          <Button type="primary" icon={<DownloadOutlined />} size={'middle'} href={downloadLink}>
          Download
        </Button>
          // <a href={downloadLink} download='video.mp4'>Descargar</a>
        )}
      </div>
    </div>

  );
}

export default WebcamRecorder;