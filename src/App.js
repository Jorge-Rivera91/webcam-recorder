/* eslint-disable react-hooks/exhaustive-deps */

import WebcamRecorder from './components/WebcamRecorder/WebcamRecorder';
import 'antd/dist/antd.css';

function App() {
  return (
    <WebcamRecorder />
  );
}

export default App;
